from django.http import JsonResponse
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.decorators import action

from car.models import Color, CarBrand, CarModel, CarOrder
from car.serializers import ColorSerializer, CarBrandSerializer, CarModelSerializer, CarOrderSerializer, \
    CarOrderSerializer1, ColorQuantitySerializer, CarBrandQuantitySerializer

from django_filters import rest_framework as dj_filters
from rest_framework.filters import OrderingFilter


class ColorViews(viewsets.ModelViewSet):
    serializer_class = ColorSerializer
    queryset = Color.objects.all()


class CarBrandViews(viewsets.ModelViewSet):
    serializer_class = CarBrandSerializer
    queryset = CarBrand.objects.all()

    @swagger_auto_schema(
        operation_description="Получение списка марок с указанием количества заказанных авто каждой марки",
        responses={200: CarBrandQuantitySerializer})
    @action(methods=['get'], detail=False)
    def get_car_brand_order(self, request):
        return JsonResponse({"data": CarBrandQuantitySerializer(instance=CarBrand.objects.all(), many=True).data})


class CarModelViews(viewsets.ModelViewSet):
    serializer_class = CarModelSerializer
    queryset = CarModel.objects.all()


class CarOrderViews(viewsets.ModelViewSet):
    queryset = CarOrder.objects.all()
    filter_backends = [dj_filters.DjangoFilterBackend, OrderingFilter]
    ordering_fields = ['quantity']
    filterset_fields = ['car_model__car_brand']

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CarOrderSerializer1
        return CarOrderSerializer

    @swagger_auto_schema(operation_description="Получение списка цветов с указанием количества заказанных авто "
                                               "каждого цвета",
                         responses={200: ColorQuantitySerializer})
    @action(methods=['get'], detail=False)
    def get_color_order(self, request):
        return JsonResponse({"data": ColorQuantitySerializer(instance=Color.objects.all(), many=True).data})

from rest_framework import serializers

from car.models import Color, CarBrand, CarModel, CarOrder


class ColorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Color
        fields = ('id', 'name')


class ColorQuantitySerializer(serializers.ModelSerializer):
    quantity = serializers.SerializerMethodField()

    class Meta:
        model = Color
        fields = ('name', 'quantity')

    def get_quantity(self, obj) -> int:
        return CarOrder.objects.filter(color=obj).count()


class CarBrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = CarBrand
        fields = ('id', 'name')


class CarBrandQuantitySerializer(serializers.ModelSerializer):
    quantity = serializers.SerializerMethodField()

    class Meta:
        model = CarBrand
        fields = ('name', 'quantity')

    def get_quantity(self, obj) -> int:
        return CarOrder.objects.filter(car_model__car_brand=obj).count()


class CarModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = CarModel
        fields = ('id', 'name', 'car_brand')


class CarOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = CarOrder
        fields = ('id', 'date', 'color',  'car_model', 'quantity')


class CarOrderSerializer1(serializers.ModelSerializer):
    color = serializers.StringRelatedField()
    car_brand = serializers.StringRelatedField(source='car_model.car_brand.name')
    car_model = serializers.StringRelatedField()

    class Meta:
        model = CarOrder
        fields = ('id', 'date', 'color', 'car_brand', 'car_model', 'quantity')


import datetime

from django.db import models
from django.utils import timezone
# Create your models here.


class Color(models.Model):
    """ Справочник цветов автомобиля """
    name = models.CharField("Название цвета", max_length=100)

    class Meta:
        verbose_name = "Цвет"
        verbose_name_plural = "Цвета"
        db_table = "Color"

    def __str__(self):
        return self.name


class CarBrand(models.Model):
    """Справочник марок автомобиля """
    name = models.CharField("Название марки", max_length=100)

    class Meta:
        verbose_name = "Марка автомобиля"
        verbose_name_plural = "Марки автомобиля"
        db_table = "CarBrand"

    def __str__(self):
        return self.name


class CarModel(models.Model):
    """Справочник моделей автомобиля """
    name = models.CharField("Название модели", max_length=100)
    car_brand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Модель автомобиля"
        verbose_name_plural = "Модели автомобиля"
        db_table = "CarModel"

    def __str__(self):
        return f'{self.car_brand.name} {self.name}'


class CarOrder(models.Model):
    """Справочник заказов автомобилей """
    color = models.ForeignKey(Color, on_delete=models.SET_NULL, null=True)
    car_model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField("Количество")
    date = models.DateTimeField('Создан', default=timezone.now(), blank=True)

    class Meta:
        verbose_name = "Модель автомобиля"
        verbose_name_plural = "Модели автомобиля"
        db_table = "CarOrder"

    def __str__(self):
        return f'{self.car_model.name} + {self.date}'
from rest_framework.routers import DefaultRouter

from car.viewsets import ColorViews, CarOrderViews, CarModelViews, CarBrandViews

urlpatterns = []


router = DefaultRouter()
router.register(r'api/car/color', ColorViews, basename='color')
router.register(r'api/car/brand', CarBrandViews, basename='brand')
router.register(r'api/car/model', CarModelViews, basename='model')
router.register(r'api/car/order', CarOrderViews, basename='order')


urlpatterns += router.urls